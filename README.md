###  Plugin Name

Contributors: Bjoern Schiessle
Tags: gnusocial, share, button
Requires at least: 4.4.2
Tested up to: 4.4.2
Stable tag: 0.1
License: GPLv3 or later
License URI: http://www.gnu.org/licenses/gpl-3.0.html

This plugin adds a "Share on GNU Social" button at the bottom of your posts.

## Description

This plugin adds a "Share on GNU Social" button at the bottom of your posts. It is based on the gs-share widget by Chimo ( https://code.chromic.org/chimo/gs-share ). It allows you to share to any GNU Social instance, either as a normal post or as a bookmark.

Here you can see it in action: http://blog.schiessle.org

## Installation

### From ZIP file

1. Unpack `wp-share-on-gnusocial.zip` and upload its contents to the `/wp-content/plugins/` directory
2. Activate the plugin through the 'Plugins' menu in WordPress

### From Git Repository

1. Go to the `/wp-content/plugins/` directory
2. `git clone http://src.schiessle.org/BeS/wp-share-on-gnusocial.git`
3. `git submodule update --init`
4. Activate the plugin through the 'Plugins' menu in WordPress
